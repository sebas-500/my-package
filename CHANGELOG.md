# CHANGELOG



## v0.4.0 (2024-03-23)

### Feature

* feat(rename): renaming project ([`4f59985`](https://gitlab.com/sebas-500/my-package/-/commit/4f5998518eeaf98b74a8fcf2d53d95bf307c2279))

### Unknown

* Merge branch &#39;main&#39; of gitlab.com:sebas-500/my-package ([`da18915`](https://gitlab.com/sebas-500/my-package/-/commit/da18915e978e5171db7fcc6639158ba6e0c96442))


## v0.3.0 (2024-03-23)

### Feature

* feat(package): adding build stage to the pipeline ([`5b81591`](https://gitlab.com/sebas-500/my-package/-/commit/5b815919eea2c642b67f85c218b8de031d88f287))

### Unknown

* Merge branch &#39;main&#39; of gitlab.com:sebas-500/my-package ([`c51212e`](https://gitlab.com/sebas-500/my-package/-/commit/c51212e6314c545559dcdda76eda7698c29a5a26))


## v0.2.0 (2024-03-23)

### Feature

* feat(test): add new test module ([`c565fa7`](https://gitlab.com/sebas-500/my-package/-/commit/c565fa7e0c842c0226911fe63b2b061ffe86d7bd))

### Unknown

* Merge branch &#39;main&#39; of gitlab.com:sebas-500/my-package ([`b590959`](https://gitlab.com/sebas-500/my-package/-/commit/b590959277f60af20338128acc25dee58c560189))


## v0.1.0 (2024-03-23)

### Feature

* feat(semantic-release): add semantic release to the repository ([`54357e7`](https://gitlab.com/sebas-500/my-package/-/commit/54357e78aa5eab9b009fae946d0a82132d1a7d87))

### Unknown

* adding poetry to the project and semver plugin ([`bfdb846`](https://gitlab.com/sebas-500/my-package/-/commit/bfdb846a8fc099ea8a4de93c00efe2c8fc95ef19))

* renaming yml ([`5419f2e`](https://gitlab.com/sebas-500/my-package/-/commit/5419f2e1a95b97ef4ff082c13bfcebdb78942d49))

* adding context to each pipeline job ([`c5f0460`](https://gitlab.com/sebas-500/my-package/-/commit/c5f0460712a1aaec9c3d2cb0346eea9b91af8525))

* adding first pipeline ([`da6fe03`](https://gitlab.com/sebas-500/my-package/-/commit/da6fe03c14c121aad71986b287f66926a51a9f51))

* Adding readme ([`035b263`](https://gitlab.com/sebas-500/my-package/-/commit/035b263cbecaeb607fe5feea2d7d50955aea5eb4))
